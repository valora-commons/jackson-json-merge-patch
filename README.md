# Jackson JSON Merge Patch

Handle more easily REST JSON Merge Patch requests with Jackson.

## Usage

### Maven

Import the latest `dev.valora.commons:jackson-json-merge-patch` artifact into your pom.xml.

```
<dependency>
	<groupId>dev.valora.commons</groupId>
	<artifactId>jackson-json-merge-patch</artifactId>
	<version>1.1.0</version>
</dependency>
```

### Service

Import service `dev.valora.commons.jacksonjsonmergepatch.service.JsonMergePatchService`.

## Requirements

* Java 8
* Spring Boot 2.2.4.RELEASE