package dev.valora.commons.jacksonjsonmergepatch.exception;

public class WrongTypeException
		extends RuntimeException {

	private static final long serialVersionUID = 1200443268405483073L;

	public WrongTypeException(String path) {
		super(path);
	}

}
