package dev.valora.commons.jacksonjsonmergepatch.service;

import java.math.BigDecimal;
import java.util.function.Consumer;

import com.fasterxml.jackson.databind.JsonNode;

import dev.valora.commons.jacksonjsonmergepatch.exception.WrongTypeException;

public class JsonMergePatchService {

	public void mergePatchText(JsonNode patchRequest, String path, Consumer<String> consumer) {
		JsonNode node = findChild(patchRequest, path);
		if (!node.isMissingNode()) {
			if (!node.isNull() && !node.isTextual()) {
				throw new WrongTypeException(path);
			}
			String value = node.textValue();
			consumer.accept(value);
		}
	}

	public void mergePatchBoolean(JsonNode patchRequest, String path, Consumer<Boolean> consumer) {
		JsonNode node = findChild(patchRequest, path);
		if (!node.isMissingNode()) {
			Boolean value;
			if (node.isNull()) {
				value = null;
			} else if (node.isBoolean()) {
				value = node.booleanValue();
			} else {
				throw new WrongTypeException(path);
			}
			consumer.accept(value);
		}
	}

	public void mergePatchInteger(JsonNode patchRequest, String path, Consumer<Integer> consumer) {
		JsonNode node = findChild(patchRequest, path);
		if (!node.isMissingNode()) {
			Integer value;
			if (node.isNull()) {
				value = null;
			} else if (node.isInt()) {
				value = node.intValue();
			} else {
				throw new WrongTypeException(path);
			}
			consumer.accept(value);
		}
	}

	public void mergePatchBigDecimal(JsonNode patchRequest, String path, Consumer<BigDecimal> consumer) {
		JsonNode node = findChild(patchRequest, path);
		if (!node.isMissingNode()) {
			BigDecimal value;
			if (node.isNull()) {
				value = null;
			} else if (node.isNumber()) {
				value = node.decimalValue();
			} else {
				throw new WrongTypeException(path);
			}
			consumer.accept(value);
		}
	}

	private JsonNode findChild(JsonNode parent, String path) {
		JsonNode currentNode = parent;
		String[] paths = path.split("\\.");
		for (String childPath : paths) {
			currentNode = currentNode.get(childPath);
		}
		return currentNode;
	}

}
